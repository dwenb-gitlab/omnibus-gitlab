## 日志

### 在服务器控制台上跟踪日志

如果您想“跟踪”，即查看GitLab日志的实时更新，您可以使用
`gitlab-ctl tail`.

```shell
# Tail all logs; press Ctrl-C to exit
sudo gitlab-ctl tail

# Drill down to a sub-directory of /var/log/gitlab
sudo gitlab-ctl tail gitlab-rails

# Drill down to an individual file
sudo gitlab-ctl tail nginx/gitlab_error.log
```

### 配置默认日志目录

在你的`/etc/gitlab/gitlab.rb`文件里, 有许多`log_directory`对应各种各样的日志。
取消注释，并更新日志的值到你想放置的地方：

```ruby
# For example:
gitlab_rails['log_directory'] = "/var/log/gitlab/gitlab-rails"
unicorn['log_directory'] = "/var/log/gitlab/unicorn"
registry['log_directory'] = "/var/log/gitlab/registry"
...
```

运行`sudo gitlab-ctl reconfigure`命令使配置生效。

### Runit日志

omnibus-gitlab上的Runit-managed服务（The Runit-managed services ）使用[svlogd][svlogd]生成日志数据。
参考[svlogd文档][svlogd]查看更多关于它生成文件的信息。

你可以通过`/etc/gitlab/gitlab.rb`文件使用下面的设置修改svlogd配置：

```ruby
# Below are the default values
logging['svlogd_size'] = 200 * 1024 * 1024 # rotate after 200 MB of log data
logging['svlogd_num'] = 30 # keep 30 rotated log files
logging['svlogd_timeout'] = 24 * 60 * 60 # rotate after 24 hours
logging['svlogd_filter'] = "gzip" # compress logs with gzip
logging['svlogd_udp'] = nil # transmit log messages via UDP
logging['svlogd_prefix'] = nil # custom prefix for log messages

# Optionally, you can override the prefix for e.g. Nginx
nginx['svlogd_prefix'] = "nginx"
```

### Logrotate

从omnibus-gitlab 7.4开始，在omnibus-gitla中有一个内置的logrotate服务。
此服务将旋转、压缩并最终删除未被Runit捕获的日志数据，如`gitlab-rails/production.log`
和`nginx/gitlab_access.log`。
你可以通过`/etc/gitlab/gitlab.rb`配置logrotate。

```
# Below are some of the default settings
logging['logrotate_frequency'] = "daily" # rotate logs daily
logging['logrotate_size'] = nil # do not rotate by size by default
logging['logrotate_rotate'] = 30 # keep 30 rotated logs
logging['logrotate_compress'] = "compress" # see 'man logrotate'
logging['logrotate_method'] = "copytruncate" # see 'man logrotate'
logging['logrotate_postrotate'] = nil # no postrotate command by default
logging['logrotate_dateformat'] = nil # use date extensions for rotated files rather than numbers e.g. a value of "-%Y-%m-%d" would give rotated files like production.log-2016-03-09.gz


# You can add overrides per service
nginx['logrotate_frequency'] = nil
nginx['logrotate_size'] = "200M"

# You can also disable the built-in logrotate service if you want
logrotate['enable'] = false
```

### UDP日志转发

如果你有一个中央服务器，在那里你所有的基础日志都被收集，
您可以配置Omnibus GitLab通过UDP发送类似syslog的日志消息:

```ruby
logging['udp_log_shipping_host'] = '1.2.3.4' # Your syslog server
logging['udp_log_shipping_port'] = 1514 # Optional, defaults to 514 (syslog)
```

日志消息示例：

```
Jun 26 06:33:46 ubuntu1204-test production.log: Started GET "/root/my-project/import" for 127.0.0.1 at 2014-06-26 06:33:46 -0700
Jun 26 06:33:46 ubuntu1204-test production.log: Processing by ProjectsController#import as HTML
Jun 26 06:33:46 ubuntu1204-test production.log: Parameters: {"id"=>"root/my-project"}
Jun 26 06:33:46 ubuntu1204-test production.log: Completed 200 OK in 122ms (Views: 71.9ms | ActiveRecord: 12.2ms)
Jun 26 06:33:46 ubuntu1204-test gitlab_access.log: 172.16.228.1 - - [26/Jun/2014:06:33:46 -0700] "GET /root/my-project/import HTTP/1.1" 200 5775 "https://172.16.228.169/root/my-project/import" "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.153 Safari/537.36"
2014-06-26_13:33:46.49866 ubuntu1204-test sidekiq: 2014-06-26T13:33:46Z 18107 TID-7nbj0 Sidekiq::Extensions::DelayedMailer JID-bbfb118dd1db20f6c39f5b50 INFO: start
2014-06-26_13:33:46.52608 ubuntu1204-test sidekiq: 2014-06-26T13:33:46Z 18107 TID-7muoc RepositoryImportWorker JID-57ee926c3655fcfa062338ae INFO: start
```

### 使用自定义NGINX日志格式

默认情况下，NGINX访问日志将使用“组合”NGINX的一个版本
格式，用于隐藏嵌入查询字符串中的潜在敏感信息。By default the NGINX access logs will use a version of the 'combined' NGINX
format, designed to hide potentially sensitive information embedded in query strings.
如果想使用自定义日志格式字符串，可以在`/etc/gitlab/gitlab.rb` - 格式细节详见
[NGINX文档](http://nginx.org/en/docs/http/ngx_http_log_module.html#log_format)。

```
nginx['log_format'] = 'my format string $foo $bar'
mattermost_nginx['log_format'] = 'my format string $foo $bar'
```

[ee]: https://about.gitlab.com/gitlab-ee/
