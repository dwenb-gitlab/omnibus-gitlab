# 更改gitlab.yml和application.yml设置

GitLab的一些特性可以通过[gitlab.yml][gitlab.yml.example]自定义配置。
如果你想通过omnibus-gitlab改变 `gitlab.yml`设置，你需要通过`/etc/gitlab/gitlab.rb`来这么做。
转换工作如下，有关可用选项的完整列表，请访问[gitlab.rb.template]。从GitLab 7.6开始的新安装
在`/etc/gitlab/gitlab.rb`中默认将会有模板的所有选项。

在 `gitlab.yml`中，你将会发现这样的结构体：

```yaml
production: &base
  gitlab:
    default_theme: 2
```

在`gitlab.rb`中，要转换成下面这样：

```ruby
gitlab_rails['gitlab_default_theme'] = 2
```

在这里发生的是我们去掉了`production: &base`，在`gitlab:`中
把`default_theme:`换成`gitlab_default_theme`。注意，
不是所有的`gitlab.yml`设置都能通过`gitlab.rb`更改：详见[gitlab.yml ERB template][gitlab.yml.erb]。
如果你认为有属性丢失，请在omnibus-gitlab仓库创建一个合并请求。

运行`sudo gitlab-ctl reconfigure`使`gitlab.rb`中的修改生效。

不要在`/var/opt/gitlab/gitlab-rails/etc/gitlab.yml`中编辑生成文件
因为在下一次`gitlab-ctl reconfigure`运行时它将被复写。

## 在`gitlab.yml`中添加新设置

当添加新设置时不要忘记更新下面的三个文件：

- 通过`/etc/gitlab/gitlab.rb`暴露给终端用户的[gitlab.rb.template]文件
- 为新设置提供合理默认值的[default.rb]文件
- 从`gitlab.rb`中实际使用设置的值的[gitlab.yml.example]文件

[gitlab.yml.example]: https://gitlab.com/gitlab-org/gitlab-ce/blob/master/config/gitlab.yml.example
[gitlab.yml.erb]: https://gitlab.com/gitlab-org/omnibus-gitlab/blob/master/files/gitlab-cookbooks/gitlab/templates/default/gitlab.yml.erb
[gitlab.rb.template]: https://gitlab.com/gitlab-org/omnibus-gitlab/blob/master/files/gitlab-config-template/gitlab.rb.template
[default.rb]: https://gitlab.com/gitlab-org/omnibus-gitlab/blob/master/files/gitlab-cookbooks/gitlab/attributes/default.rb
