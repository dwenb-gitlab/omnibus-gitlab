# 配置选项

GitLab通过在`/etc/gitlab/gitlab.rb`中设置相关选项进行配置。默认的设置列表可在[package defaults](../package-information/defaults.md)中查看。
全部可配置选项列表查看[gitlab.rb.template](https://gitlab.com/gitlab-org/omnibus-gitlab/blob/master/files/gitlab-config-template/gitlab.rb.template)。
从GitLab7.6开始启用新的配置，在`/etc/gitlab/gitlab.rb`中默认展示全部的可配置列表项。

## 为GitLab配置外部URL

为了让GitLab向用户显示正确的仓库链接，需要配置用户可访问的URL，e.g.`http://gitlab.example.com`，可在`/etc/gitlab/gitlab.rb`中添加或修改如下行：

```ruby
external_url "http://gitlab.example.com"
```

然后运行`sudo gitlab-ctl reconfigure`使配置生效。

## 为Gitlab配置相对地址

_**注意：**在Omnibus GitLab中支持的相对地址正在**试用**，将在8.5版本中
[正式引入][590]。对于源配置可参考单独的
[文档](https://docs.gitlab.com/ce/install/relative_url.html)。_

---

虽然推荐在自己的（子）域中安装GitLab，但有时因为各种原因，需要把GitLab安装在一个相对URL下，
例如`https://example.com/gitlab`。

注意，更改URL后所有的远程URL都会改变，所以你得在任何指向你GitLab实例的本地存储库中手动修改它们。

### 相对URL配置需求

_从8.17包开始，就**不再需要重新编译资源**._

Omnibus GitLab包附带预编译资源(CSS, JavaScript,fonts, etc.)。如果你运行一个_8.17以前_ 的包，
然后Omnibus配置的是相对URL，资源就需要重新编译，这时就是一个需要消耗大量CPU和内存资源的任务。 
为了避免内存不足，最少需要2GB可用的RAM，当然，我们推荐的是4GB RAM，4核心或8核心CPU。

### 在GitLab上启用相对URL

下面是在GitLab上启用相对URL的步骤：

1. （可选）如果你的资源不足，可用通过以下命令关闭Unicorn和Sidekiq以暂时释放一些内存：

    ```shell
    sudo gitlab-ctl stop unicorn
    sudo gitlab-ctl stop sidekiq
    ```

1. 在中`/etc/gitlab/gitlab.rb`设置`external_url`：

    ```ruby
    external_url "https://example.com/gitlab"
    ```

    在这个例子中，GitLab提供的相对URL是`/gitlab`。可以自行修改路径。

1. 重新配置GitLab以使修改生效：

    ```shell
    sudo gitlab-ctl reconfigure
    ```

1. 重启服务让Unicorn和Sidekiq获取这些改变：

    ```shell
    sudo gitlab-ctl restart
    ```

如果遇到问题，请参阅[故障诊断和排除]
(#relative-url-troubleshooting)。

### 在GitLab上禁用相对URL

禁用相对URL，和上面的启用步骤一样，不过要把`external_url`设置成不包含相对路径的地址。
在启用新配置命令之后，可能需要明确的重启Unicorn：

```shell
sudo gitlab-ctl restart unicorn
```

如果遇到问题，请参阅[故障诊断和排除]
(#relative-url-troubleshooting)。

### 相对URL故障诊断和排除

在使用相对URL的配置后如果发现任何资源展示的问题（例如图像缺失或组件无反应）
请在[GitLab CE](https://gitlab.com/gitlab-org/gitlab-ce)
上用`Frontend`标签提问。

如果你运行一个_8.17以前_的版本，并且因为一些原因资源编译步骤出错（也就是服务器内存耗尽），
你可以在解决问题后（例如增加swap分区大小）手动执行任务：

```shell
sudo NO_PRIVILEGE_DROP=true USE_DB=false gitlab-rake assets:clean assets:precompile
sudo chown -R git:git /var/opt/gitlab/gitlab-rails/tmp/cache
```

在`gitlab.rb`中修改了默认的`user['username']`，`user['group']`和`gitlab_rails['dir']`
用户和路径可能也会有所不同，这时要确保上面的`chown`命令运行在正确的用户名和组上。

[590]: https://gitlab.com/gitlab-org/omnibus-gitlab/merge_requests/590 "Merge request - Relative url support for omnibus installations"

## 从非root用户中载入外部配置文件

Omnibus-gitlab包所有的配置都从`/etc/gitlab/gitlab.rb`里加载。这个文件具有严格的文件权限，且由`root`用户拥有。
严格执行权限的原因是在运行`gitlab-ctl reconfigure`时`/etc/gitlab/gitlab.rb`被`root`用户当做Ruby语言执行。
这就意味着对`/etc/gitlab/gitlab.rb`拥有写权限的用户，可以增加配置并且以`root`身份执行代码。

在某些组织中允许非根用户配置文件。你可以在`/etc/gitlab/gitlab.rb`中通过指定文件路径引入一个外部配置文件：

```ruby
from_file "/home/admin/external_gitlab.rb"

```

请注意：在运行`sudo gitlab-ctl reconfigure`时，你用`from_file`引入`/etc/gitlab/gitlab.rb`的代码将以`root`身份运行。
在`/etc/gitlab/gitlab.rb`里`from_file`之后设置的任何配置都比引入的文件有更高的优先级。

## 将Git数据存储在其他目录

默认的，omnibus-gitlab将Git仓库的数据存储在`/var/opt/gitlab/git-data`目录下。具体的仓库则存储在子目录
`repositories`中。你可以通过在`/etc/gitlab/gitlab.rb`中增加下面这行来修改 `git-data`父目录位置。

```ruby
git_data_dirs({ "default" => { "path" => "/mnt/nas/git-data" } })
```

你也可以通过在`/etc/gitlab/gitlab.rb`中添加如下行来增加多个git数据目录。

```ruby
git_data_dirs({
  "default" => { "path" => "/var/opt/gitlab/git-data" },
  "alternative" => { "path" => "/mnt/nas/git-data" }
})
```

注意目标目录和任何子路径不能是符号链接。

运行`sudo gitlab-ctl reconfigure`使修改生效。

如果你在`/var/opt/gitlab/git-data`中有已经存在的Git仓库，你可以按下列操作把它们迁移到新的位置：

```shell
# Prevent users from writing to the repositories while you move them.
sudo gitlab-ctl stop

# Note there is _no_ slash behind 'repositories', but there _is_ a
# slash behind 'git-data'.
sudo rsync -av /var/opt/gitlab/git-data/repositories /mnt/nas/git-data/

# Start the necessary processes and run reconfigure to fix permissions
# if necessary
sudo gitlab-ctl upgrade

# Double-check directory layout in /mnt/nas/git-data. Expected output:
# repositories
sudo ls /mnt/nas/git-data/

# Done! Start GitLab and verify that you can browse through the repositories in
# the web interface.
sudo gitlab-ctl start
```

## 更改Git用户/组的名称

默认的omnibus-gitLab使用`git`作为Git gitlab-shell的登录用户名，并拥有Git数据的所有权，
同时在web界面生成SSH URL。类似的，用`git`组用来管理Git数据的组权限。

我们不建议更改现有的安装用户/组，因为可能会出现不可预知的副作用。如果你坚持想改用户和组，
在`/etc/gitlab/gitlab.rb`中可以这样做：

```ruby
user['username'] = "gitlab"
user['group'] = "gitlab"
```

运行`sudo gitlab-ctl reconfigure`启用修改的配置。

注意，如果您更改了现有安装的用户名，那么重新配置运行将不会改变嵌套目录的所有权，因此您必须手动操作。
确保新用户能访问确保新用户能访问`仓库`和`上传`目录。

## 指定数字用户和组标识

omnibus-gitlab为GitLab，PostgreSQL，Redis和NGINX创建用户。你可以按如下方法在`/etc/gitlab/gitlab.rb`中为这些用户指定数字表示：

```ruby
user['uid'] = 1234
user['gid'] = 1234
postgresql['uid'] = 1235
postgresql['gid'] = 1235
redis['uid'] = 1236
redis['gid'] = 1236
web_server['uid'] = 1237
web_server['gid'] = 1237
```

运行`sudo gitlab-ctl reconfigure`使配置生效。

## 禁用用户和组账户管理

默认情况下，omnibus-gitlab负责创建系统用户和组帐户，以及保持信息的更新。
这些系统帐户运行包的各种组件。大多数用户不需要改变这些行为。然而，如果你的
系统账户被其他的软件管理，例如：LDAP，你可能需要通过包来禁用账户管理。

为了禁用用户和组账户管理，在中`/etc/gitlab/gitlab.rb`设置：

```ruby
manage_accounts['enable'] = false
```

*警告* Omnibus-gitlab仍然希望用户和组在omnibus-gitlab包安装的系统中存在。

默认情况，omnibus-gitlab包期望一下用户能存在：


```bash
# GitLab user (required)
git

# Web server user (required)
gitlab-www

# Redis user for GitLab (only when using packaged Redis)
gitlab-redis

# Postgresql user (only when using packaged Postgresql)
gitlab-psql

# Prometheus user for prometheus monitoring and various exporters
gitlab-prometheus

# GitLab Mattermost user (only when using GitLab Mattermost)
mattermost

# GitLab Registry user (only when using GitLab Registry)
registry

# GitLab Consul user (only when using GitLab Consul)
gitlab-consul
```

默认情况，omnibus-gitlab包期望一下用户组存在：

```bash
# GitLab group (required)
git

# Web server group (required)
gitlab-www

# Redis group for GitLab (only when using packaged Redis)
gitlab-redis

# Postgresql group (only when using packaged Postgresql)
gitlab-psql

# Prometheus user for prometheus monitoring and various exporters
gitlab-prometheus

# GitLab Mattermost group (only when using GitLab Mattermost)
mattermost

# GitLab Registry group (only when using GitLab Registry)
registry

# GitLab Consul group (only when using GitLab Consul)
gitlab-consul
```

你也可以使用不同的用户/组的名称，但是那样你要在`/etc/gitlab/gitlab.rb`中指定用户/组的细节，例如：

```ruby
# Do not manage user/group accounts
manage_accounts['enable'] = false

# GitLab
user['username'] = "custom-gitlab"
user['group'] = "custom-gitlab"
user['shell'] = "/bin/sh"
user['home'] = "/var/opt/custom-gitlab"

# Web server
web_server['username'] = 'webserver-gitlab'
web_server['group'] = 'webserver-gitlab'
web_server['shell'] = '/bin/false'
web_server['home'] = '/var/opt/gitlab/webserver'

# Postgresql (not needed when using external Postgresql)
postgresql['username'] = "postgres-gitlab"
postgresql['shell'] = "/bin/sh"
postgresql['home'] = "/var/opt/postgres-gitlab"

# Redis (not needed when using external Redis)
redis['username'] = "redis-gitlab"
redis['shell'] = "/bin/false"
redis['home'] = "/var/opt/redis-gitlab"

# And so on for users/groups for GitLab Mattermost
```

## 禁用存储目录管理

omnibus-gitlab包负责创建所有拥有正确的所有权和权限且必要的目录，并保持更新。

一些必要的目录中会包含大量的数据，所以在某些设置中，
这些目录很可能安装在NFS(或其他)共享上。

某些类型的挂载不允许根用户（初始设置的默认用户）自动创建目录，例如：eg. NFS with 启用`root_squash`的NFS共享。
作为变通，omnibus-gitlab包将使用目录的所有者用户来创建那些目录。

如果你有`/etc/gitlab`目录挂载，你可以关闭那个目录的管理。

在`/etc/gitlab/gitlab.rb`中设置：

```ruby
manage_storage_directories['manage_etc'] = false
```

如果你正在挂载所有的GitLab存储目录，对于每一个挂载，你都应该完全的禁用存储目录管理。

为了禁用这些目录，在`/etc/gitlab/gitlab.rb`中设置：

```ruby
manage_storage_directories['enable'] = false
```

**警告** omnibus-gitlab包仍然期望那些目录在文件系统中存在。如果这些配置被正确的设置，
那些目录将由管理员创建并设置正确的权限。

启用这些设置将阻止创建下列目录：

| Default location | Permissions | Ownership | Purpose |
| ---------------- | ----------- | --------- | ------- |
| `/var/opt/gitlab/git-data`   | 0700 | git:root | Holds repositories directory |
| `/var/opt/gitlab/git-data/repositories` | 2770 | git:git | Holds git repositories |
| `/var/opt/gitlab/gitlab-rails/shared` | 0751 | git:gitlab-www | Holds large object directories |
| `/var/opt/gitlab/gitlab-rails/shared/artifacts` | 0700 | git:root | Holds CI artifacts |
| `/var/opt/gitlab/gitlab-rails/shared/lfs-objects` | 0700 | git:root | Holds LFS objects |
| `/var/opt/gitlab/gitlab-rails/uploads` | 0700 | git:root | Holds user attachments |
| `/var/opt/gitlab/gitlab-rails/shared/pages` | 0750 | git:gitlab-www | Holds user pages |
| `/var/opt/gitlab/gitlab-ci/builds` | 0700 | git:root | Holds CI build logs |
| `/var/opt/gitlab/.ssh` | 0700 | git:git | Holds authorized keys |



## 只有给定的文件系统被挂载，才能启动omnibus-gitlab服务

如果你想从启动一个给定的文件系统开始就阻止omnibus-gitlab服务(NGINX, Redis, Unicorn etc.)
在`/etc/gitlab/gitlab.rb`中添加下面这行：

```ruby
# wait for /var/opt/gitlab to be mounted
high_availability['mountpoint'] = '/var/opt/gitlab'
```

运行`sudo gitlab-ctl reconfigure`使配置生效。

## 配置运行时目录

当Prometheus监听启用后，GitLab-monitor将管理度量每一个Unicorn进程(Rails metrics)。
每一个Unicorn的进程都需要对每一个控制器请求都需要写一个度量文件到一个临时位置。
Prometheus将收集这些文件并处理他们的值。

为了避免创建磁盘I/O，omnibus-gitlab包将使用一个运行时目录。

在`reconfigure`运行时, 包将检查`/run`是否是一个`tmpfs`挂载。
如果不是，就打印警告：

```
Runtime directory '/run' is not a tmpfs mount.
```

然后Rails权值会被禁用。

为了让Rails权值再次启用，需要创建一个`tmpfs`并在`/etc/gitlab/gitlab.rb`中指定：

```
runtime_dir '/path/to/tmpfs'
```

*请注意在配置中没有`=`。*

运行`sudo gitlab-ctl reconfigure`是配置生效。

## 配置Rack攻击

使用了rack-attack gem防止客户端滥用造成破坏。
点击[这里](https://gitlab.com/help/security/rack_attack.md)
查看更多信息。

`config/initializers/rack_attack.rb`文件被omnibus-gitlab管理，且必须在`/etc/gitlab/gitlab.rb`中配置。

## 在安装期间禁用自动缓存清理

如果您有大型gitlab安装，您可能不希望运行`rake cache:clean`任务。因为要花很长时间才能完成。
默认情况下，再重新配置期间缓存清除任务将自动运行。

编辑`/etc/gitlab/gitlab.rb`：

```ruby
# This is advanced feature used by large gitlab deployments where loading
# whole RAILS env takes a lot of time.
gitlab_rails['rake_cache_clear'] = false
```

不要忘记删除这行开头的`#`注释字符。

### 启用/禁用Rack攻击和安装基本的认证节流

下面的配置设置控制Rack攻击：

```ruby
gitlab_rails['rack_attack_git_basic_auth'] = {
  'enabled' => true, # Enable/Disable Rack Attack
  'ip_whitelist' => ["127.0.0.1"], # Whitelisted urls
  'maxretry' => 10, # Limit the number of Git HTTP authentication attempts per IP
  'findtime' => 60, # Reset the auth attempt counter per IP after 60 seconds
  'bantime' => 3600 # Ban an IP for one hour (3600s) after too many auth attempts
}
```

### 设置受Rack攻击保护的路径

如果你想更改默认的保护路径，在配置文件中设置`gitlab_rails['rack_attack_protected_paths']`。

**警告** 这个操作将复写omnibus-gitlab提供的清单：

```ruby
gitlab_rails['rack_attack_protected_paths'] = [
  '/users/password',
  '/users/sign_in',
  '/api/#{API::API.version}/session.json',
  '/api/#{API::API.version}/session',
  '/users',
  '/users/confirmation',
  '/unsubscribes/',
  '/import/github/personal_access_token'
]
```

_**注意：**所有的路径都是相对gitlab的url._
如果你设置了它，请不要包含[相对URL](configuration.md#configuring-a-relative-url-for-gitlab)。

**警告**如果路径包含需要由rail修改的变量(ex. `#{API::API.version}`)那时你需要转义大括号或者使用单引号字符串。
例如`"/api/#\{API::API.version\}/session.json"`或`'/api/#{API::API.version}/session.json'`


### 为'被保护路径'安装节流
使用下面的选项来控制'limit'和 'period'节流：

```ruby
gitlab_rails['rate_limit_requests_per_period'] = 10
gitlab_rails['rate_limit_period'] = 60
```

运行`sudo gitlab-ctl reconfigure`使配置生效。

## Setting up LDAP sign-in

See [doc/settings/ldap.md](ldap.md).

## Enable HTTPS

See [doc/settings/nginx.md](nginx.md#enable-https).

### Redirect `HTTP` requests to `HTTPS`.

See [doc/settings/nginx.md](nginx.md#redirect-http-requests-to-https).

### Change the default port and the ssl certificate locations.

See
[doc/settings/nginx.md](nginx.md#change-the-default-port-and-the-ssl-certificate-locations).

## Use non-packaged web-server

For using an existing Nginx, Passenger, or Apache webserver see [doc/settings/nginx.md](nginx.md#using-a-non-bundled-web-server).

## Using a non-packaged PostgreSQL database management server

To connect to an external PostgreSQL or MySQL DBMS see [doc/settings/database.md](database.md) (MySQL support in the Omnibus Packages is Enterprise Only).

## Using a non-packaged Redis instance

See [doc/settings/redis.md](redis.md).

## Adding ENV Vars to the GitLab Runtime Environment

See
[doc/settings/environment-variables.md](environment-variables.md).

## Changing GitLab.yml settings

See [doc/settings/gitlab.yml.md](gitlab.yml.md).

## Sending application email via SMTP

See [doc/settings/smtp.md](smtp.md).

## Omniauth (Google, Twitter, GitHub login)

Omniauth configuration is documented in
[docs.gitlab.com](https://docs.gitlab.com/ce/integration/omniauth.html).

## Adjusting Unicorn settings

See [doc/settings/unicorn.md](unicorn.md).

## Setting the NGINX listen address or addresses

See [doc/settings/nginx.md](nginx.md).

## Inserting custom NGINX settings into the GitLab server block

See [doc/settings/nginx.md](nginx.md).

## Inserting custom settings into the NGINX config

See [doc/settings/nginx.md](nginx.md).

## Enable nginx_status

See [doc/settings/nginx.md](nginx.md).
