# 设置自定义环境变量

如果有需要，你可以通过`/etc/gitlab/gitlab.rb`设置供Unicorn，Sidekiq，
Rails and RakeIf使用的环境变量。这在你需要使用代理接入网络并且希望直接
将外部托管的存储库克隆到gitlab中的时候很有用。
在`/etc/gitlab/gitlab.rb`中提供一个带哈希值的`gitlab_rails['env']`。例如：

```ruby
gitlab_rails['env'] = {"http_proxy" => "my_proxy", "https_proxy" => "my_proxy"}
```

## 修改生效

任何环境变量的修改在reconfigure后需要 **硬重启** 才能生效。

**`注意`**：在硬重启期间，你的Gitlab实例将被停止，直到服务恢复。

所以，在编辑`gitlab.rb`文件之后，运行下面的命令

```shell
sudo gitlab-ctl reconfigure
sudo gitlab-ctl restart
```
