# NGINX配置

## 启用HTTPS

### 警告

Nignx配置将会在未来24个月内只能通过一个安全的Gitlab实例让浏览器与客户端通信，
你需要为你的实例在未来至少24个月内通过启用HTTPS提供一个安全连接。

默认情况omnibus-gitlab并不使用HTTPS。如果你想为你的gitlab.example.com启用HTTPS，在`/etc/gitlab/gitlab.rb`中添加下面的声明：

```ruby
# note the 'https' below
external_url "https://gitlab.example.com"
```

因为在我们的例子中主机名是'gitlab.example.com'，omnibus-gitlab
将会分别寻找名为`/etc/gitlab/ssl/gitlab.example.com.key`和`/etc/gitlab/ssl/gitlab.example.com.crt`的key和certificate文件。
创建`/etc/gitlab/ssl`目录，将你的key和certificate复制到目录下。

```
sudo mkdir -p /etc/gitlab/ssl
sudo chmod 700 /etc/gitlab/ssl
sudo cp gitlab.example.com.key gitlab.example.com.crt /etc/gitlab/ssl/
```

现在运行`sudo gitlab-ctl reconfigure`。当配置完成后你的Gitlab实例将可以访问`https://gitlab.example.com`。

如果您开启了防火墙，您可能需要打开端口443来允许HTTPS流量入站。

```
# UFW example (Debian, Ubuntu)
sudo ufw allow https

# lokkit example (RedHat, CentOS 6)
sudo lokkit -s https

# firewall-cmd (RedHat, Centos 7)
sudo firewall-cmd --permanent --add-service=https
sudo systemctl reload firewalld
```

## 将`HTTP`重定向到`HTTPS`

默认情况，当你指定了一个以'https'开头的外部URL，Nginx将不再监听80端口的非加密HTTP流量。
 如果你想将HTTP重定向到HTTPS，你可以使用`redirect_http_to_https`配置。

```ruby
external_url "https://gitlab.example.com"
nginx['redirect_http_to_https'] = true
```

## 更改缺省端口和SSL证书的位置

如果您需要使用一个HTTPS端口，而不是默认的(443)，只需要把它设为外部URL的一部分即可。

```ruby
external_url "https://gitlab.example.com:2443"
```

创建`/etc/gitlab/ssl`目录来设置ssl证书的位置，
把`.crt`和`.key`文件放在目录中，并指定下面的配置：

```ruby
# For GitLab
nginx['ssl_certificate'] = "/etc/gitlab/ssl/gitlab.example.com.crt"
nginx['ssl_certificate_key'] = "/etc/gitlab/ssl/gitlab.example.com.key"
```

运行`sudo gitlab-ctl reconfigure`是配置生效。

## 更改缺省代理头部

默认情况，当你指定了`external_url`，omnibus-gitlab将设置适应大多数环境的NGINX代理头部。

例如，如果你在`external_url`指定了`https`模式，omnibus-gitlab将这样设置：

```
  "X-Forwarded-Proto" => "https",
  "X-Forwarded-Ssl" => "on"
```

然而，在你的GitLab中有更复杂的安装情景如反向代理，
你需要按顺序调整代理头部来避免像 `The change you wanted was rejected`或
`Can't verify CSRF token authenticity Completed 422 Unprocessable`的错误。

可以通过覆盖默认的头部来实现这一点，例如在`/etc/gitlab/gitlab.rb`中指定：

```ruby
 nginx['proxy_set_headers'] = {
  "X-Forwarded-Proto" => "http",
  "CUSTOM_HEADER" => "VALUE"
 }
```

保存文件并[reconfigure GitLab](https://docs.gitlab.com/ce/administration/restart_gitlab.html#omnibus-gitlab-reconfigure)
以使配置生效。

通过这种方式，你可以指定需要NGINX支持的任何消息头。

## 配置GitLab `trusted_proxies`和NGINX `real_ip`模块

默认情况下，NGINX和GitLab将记录连接客户端的IP地址。

如果你的GitLab是一个反向代理，你可能不想将代理IP地址显示为客户端地址。

你可以通过让NGINX将你的反向代理添加到`real_ip_trusted_addresses`列表来寻找不通的地址：

```ruby
# Each address is added to the the NGINX config as 'set_real_ip_from <address>;'
nginx['real_ip_trusted_addresses'] = [ '192.168.1.0/24', '192.168.2.1', '2001:0db8::/32' ]
# other real_ip config options
nginx['real_ip_header'] = 'X-Real-IP'
nginx['real_ip_recursive'] = 'on'
```

这些选项的说明：
* http://nginx.org/en/docs/http/ngx_http_realip_module.html

默认情况，omnibus-gitlab将使用`real_ip_trusted_addresses`中的IP地址作为
GitLab的可信代理，使用户不会显示在登录IP列表中（？）。

保存文件并[reconfigure GitLab](https://docs.gitlab.com/ce/administration/restart_gitlab.html#omnibus-gitlab-reconfigure)
是配置生效。

## 配置HTTP2协议

默认情况，通过指定`external_url "https://gitlab.example.com"`，
当你的GitLab实例可以通过HTTPS访问时，
[http2 protocol]也就已经启用了。

Omnibus-gitlab包设置需要的是与https协议兼容的ssl_cipher。

如果你在你的配置中指定了自定义的ssl_cipher，而密码在[http2 cipher blacklist]中（？），
一旦你尝试连接你的GitLab实例你的浏览器将提示`INADEQUATE_SECURITY`错误。

只有当你有一个非常特定的自定义设置时才有必要改变密码，并考虑从密码列表中删除违规的密码（？）。

关于为什么要启用http2协议的更多信息，查阅[http2 whitepaper]。

如果不想更改密码，你可以禁用在`/etc/gitlab/gitlab.rb`中禁用https支持：

```ruby
nginx['http2_enabled'] = false
```

保存文件并[reconfigure GitLab](https://docs.gitlab.com/ce/administration/restart_gitlab.html#omnibus-gitlab-reconfigure)
使配置生效。

## 使用非内置的webserver

Omnibus-gitlab默认内置Nginx服务器。
Omnibus-gitlab运行webserver通过用户`gitlab-www`接入（组名同用户名）。
为了允许外部web服务器接入GitLab，webserver用户需要添加到`gitlab-www`组中。

使用其他的web服务器像Apache或已存在的Nginx安装，你必须执行以下步骤：

1. **禁用内置Nginx**

    在`/etc/gitlab/gitlab.rb`中设置：

    ```ruby
    nginx['enable'] = false
    ```

1. **设置非内置web服务器用户的用户名**

    默认情况下，omnibus-gitlab没有为外部webserver用户设置默认设置。你不得不在配置中指定。
     对于Debian/Ubuntu，Apache/Nginx默认用户都是`www-data`，然而对于RHEL/CentOS Nginx用户则是`nginx`。

    *注意：首先确保你安装了apach/nginx，以便创建webserver用户，否则reconfiguring时会失败。*

    比方说webserver的用户是`www-data`，在`/etc/gitlab/gitlab.rb`中这样设置：

    ```ruby
    web_server['external_users'] = ['www-data']
    ```

    *注意：这里的设置是一个数组，所以你可以添加多个用户到gitlab-www组中。*

    运行`sudo gitlab-ctl reconfigure`是配置生效。

    *注意：如果你使用了SELinux并且web服务器在严格SELinux配置模式下运行，你将不得不[loosen the restrictions on your web server][selinuxmod].*

    *注意：确保webserver用户在外部web服务器上对使用的所有目录都有正确的权限，
    否则你将收到`failed (XX: Permission denied) while reading upstream`错误。

1. **将非内置web服务器添加到可信代理列表中**

    通常，omnibus-gitlab在内置NGINX的real_ip模块配置可信代理列表。

    对于非内置服务器，可信代理列表需要直接配置，如果和GitLab不在同一台服务器上还需包含web服务器的IP地址。
    否则用户将被显示为从你的web服务器的IP地址上登录。

    ```ruby
    gitlab_rails['trusted_proxies'] = [ '192.168.1.0/24', '192.168.2.1', '2001:0db8::/32' ]
    ```

1. **(可选)设置正确的gitlab-workhorse配置（如果使用Apache）**

    *注意：下面的值在GitLab8.2中添加，确保你安装了最新的版本。*

    Apache不能连接到UNIX套接字，而是需要连接一个TCP端口。
    在`/etc/gitlab/gitlab.rb`编辑允许gitlab-workhorse在TCP上监听（默认8181端口）。

    ```
    gitlab_workhorse['listen_network'] = "tcp"
    gitlab_workhorse['listen_addr'] = "127.0.0.1:8181"
    ```

    运行`sudo gitlab-ctl reconfigure`是配置生效。

1. **下载正确的web服务器配置**

    到[GitLab recipes repository][recipes-web]寻找你选择的web服务器目录的综合配置。
    依赖你是否选择带SSL的GitLab服务确保你使用了正确的配置。
    你唯一需要改变的是在`YOUR_SERVER_FQDN`中使用你自己的FQDN，如果你用了SSL，用你目前的SSL keys位置替换。
    你也可能需要改变你的日志文件的位置。

## 设置NGINX监听地址

NGINX默认会接收所有本地IPv4地址的传入连接。
你可以在`/etc/gitlab/gitlab.rb`中改变地址列表。

```ruby
nginx['listen_addresses'] = ["0.0.0.0", "[::]"] # listen on all IPv4 and IPv6 addresses
```

## 设置NGINX监听端口

By default NGINX默认会监听在`external_url`中指定的端口或者隐式地使用正确的端口(HTTP为80，HTTPS为443)。
如果你在一个反向代理上运行GitLab，你可能想复写监听端口来做一些事情。例如：使用8081端口：

```ruby
nginx['listen_port'] = 8081
```

## 支持SSL代理

如果`external_url`包含了`https://`NGINX会默认检测是否使用了SSL。
如果你在一个反向代理上运行GitLab，你肯能希望在另一个代理服务器或负载均衡上终止SSL。
为了做到这样，请确保`external_url`中包含了`https://`，并且在`gitlab.rb`中应用下列配置：

```ruby
nginx['listen_port'] = 80
nginx['listen_https'] = false
nginx['proxy_set_headers'] = {
  "X-Forwarded-Proto" => "https",
  "X-Forwarded-Ssl" => "on"
}
```

注意，您可能需要配置反向代理或负载平衡器
转发某些标头（例如：`Host`，`X-Forwarded-Ssl`，`X-Forwarded-For`，`X-Forwarded-Port`）到GitLab(如果你使用的话这点很重要)。 
如果你忘了这步，你可能会遇到不正确的重定向或错误（例如："422 Unprocessable Entity"，"Can't verify CSRF token authenticity"）。
更多信息，详见：

* http://stackoverflow.com/questions/16042647/whats-the-de-facto-standard-for-a-reverse-proxy-to-tell-the-backend-ssl-is-used
* https://wiki.apache.org/couchdb/Nginx_As_a_Reverse_Proxy

## 设置HTTP严格的传输安全

GitLab默认启用严格的传输安全，并通知浏览器应该只能通过HTTPS来与网站交流。 
当用户显式的输入`http://`地址，浏览器还是会访问GitLab实例，即便它记得不要尝试不安全的连接，
这样的地址会被浏览器自动的重定向转化为`https://`。

```ruby
nginx['hsts_max_age'] = 31536000
nginx['hsts_include_subdomains'] = false
```

`max_age`默认设置为一年。这是浏览器记得只能通过HTTPS连接的时间。
将`max_age`设置为0将禁用该特性。更多信息详见：

* https://www.nginx.com/blog/http-strict-transport-security-hsts-and-nginx/

## 使用自定义SSL密码

默认情况下，GitLab使用的是在gitlab.com上进行测试的SSL加密器，以及GitLab社区提供的各种最佳实践。

然而，你可以在`gitlab.rb`中添加下面信息来修改ssl密码：

```ruby
  nginx['ssl_ciphers'] = "CIPHER:CIPHER1"
```

然后运行新的配置。

你也可以直接启用`ssl_dhparam`。

首先，用`openssl dhparam -out dhparams.pem 2048`生成`dhparams.pem`。然后，在`gitlab.rb`中添加生成的文件路径，例如：

```ruby
  nginx['ssl_dhparam'] = "/etc/gitlab/ssl/dhparams.pem"
```

修改后运行`sudo gitlab-ctl reconfigure`.

## 启用2路SSL客户端验证

要求web客户端使用受信任的证书进行身份验证，你可以通过在`gitlab.rb`中添加下面的配置来启用2路SSL：

```ruby
  nginx['ssl_verify_client'] = "on"
```

然后运行新的配置。

NGINX支持的配置SSL客户端认证的其他选项也可以配置：

```ruby
  nginx['ssl_client_certificate'] = "/etc/pki/tls/certs/root-certs.pem"
  nginx['ssl_verify_depth'] = "2"
```

修改后运行`sudo gitlab-ctl reconfigure`。

## 将定制的NGINX设置插入到GitLab服务器块中

记住，如果在`gitlab.rb`中定义了相同的配置，那么那些自定义配置可能会引起冲突。

如果因为一些原因，你需要为GitLab添加自定义设置到NGINX`server`块中，你可以使用下面的配置：

```ruby
# Example: block raw file downloads from a specific repository
nginx['custom_gitlab_server_config'] = "location ^~ /foo-namespace/bar-project/raw/ {\n deny all;\n}\n"
```

运行`gitlab-ctl reconfigure`重写NGINX配置，并重启NGINX。

将定义的字符串添加到`/var/opt/gitlab/nginx/conf/gitlab-http.conf`中`server`块末尾。

### 注意：

+ 如果你添加了新的位置，你需要在字符串或nginx包含配置中包含

    ```conf
    proxy_cache off;
    proxy_pass http://gitlab-workhorse;
    ```

    没有这些，任何sub-location将返回404错误。详见
    [gitlab-ce#30619](https://gitlab.com/gitlab-org/gitlab-ce/issues/30619).
+ 你不能添加根路径`/`和`/assets`路径，因为这些在`gitlab-http.conf`中已经存在了。

## 在NGINX配置中插入自定义设置

如果你需要在NGINX配置中添加自定义设置，例如，包含已存在的服务块，你可以使用下面 的配置：

```ruby
# Example: include a directory to scan for additional config files
nginx['custom_nginx_config'] = "include /etc/nginx/conf.d/*.conf;"
```

运行`gitlab-ctl reconfigure`重写NGINX配置，并重启NGINX。

将定义的字符串添加在`/var/opt/gitlab/nginx/conf/nginx.conf`中的`http`块末尾。

## 自定义error页

你可以使用`custom_error_pages`在GitLab的默认error页调整文本。
这些可以在任何有效的HTTP错误页中被使用，如：404，502。

下面的内容作为一个修改404错误页的例子：

```ruby
nginx['custom_error_pages'] = {
  '404' => {
    'title' => 'Example title',
    'header' => 'Example header',
    'message' => 'Example message'
  }
}
```

这将导致404错误页面变成下面这样。

![custom 404 error page](img/error_page_example.png)

运行`gitlab-ctl reconfigure`重写NGINX配置，并重启NGINX。

## 使用现有的Passenger/Nginx安装

在一些情况下，你想让GitLab使用现有的Passenger/Nginx安装，
但是仍然具有omnibus包的安装和更新的方便性。

### 配置

首先，你需要设置你的`/etc/gitlab/gitlab.rb`，禁用内置的Nginx和Unicorn：

```ruby
# Define the external url
external_url 'http://git.example.com'

# Disable the built-in nginx
nginx['enable'] = false

# Disable the built-in unicorn
unicorn['enable'] = false

# Set the internal API URL
gitlab_rails['internal_api_url'] = 'http://git.example.com'

# Define the web server process user (ubuntu/nginx)
web_server['external_users'] = ['www-data']
```

确保运行`sudo gitlab-ctl reconfigure`是配置生效。

**注意：**如果你运行的版本低于8.16.0，如果存在unicorn服务文件（`/opt/gitlab/service/unicorn`），
 你将不得不手动删除它，然后重新启用配置。

### 虚拟主机（server block）

然后，在你的自定义Passenger/Nginx安装中，创建下面的网站配置文件：

```
upstream gitlab-workhorse {
  server unix://var/opt/gitlab/gitlab-workhorse/socket fail_timeout=0;
}

server {
  listen *:80;
  server_name git.example.com;
  server_tokens off;
  root /opt/gitlab/embedded/service/gitlab-rails/public;

  client_max_body_size 250m;

  access_log  /var/log/gitlab/nginx/gitlab_access.log;
  error_log   /var/log/gitlab/nginx/gitlab_error.log;

  # Ensure Passenger uses the bundled Ruby version
  passenger_ruby /opt/gitlab/embedded/bin/ruby;

  # Correct the $PATH variable to included packaged executables
  passenger_env_var PATH "/opt/gitlab/bin:/opt/gitlab/embedded/bin:/usr/local/bin:/usr/bin:/bin";

  # Make sure Passenger runs as the correct user and group to
  # prevent permission issues
  passenger_user git;
  passenger_group git;

  # Enable Passenger & keep at least one instance running at all times
  passenger_enabled on;
  passenger_min_instances 1;

  location ~ ^/[\w\.-]+/[\w\.-]+/(info/refs|git-upload-pack|git-receive-pack)$ {
    # 'Error' 418 is a hack to re-use the @gitlab-workhorse block
    error_page 418 = @gitlab-workhorse;
    return 418;
  }

  location ~ ^/[\w\.-]+/[\w\.-]+/repository/archive {
    # 'Error' 418 is a hack to re-use the @gitlab-workhorse block
    error_page 418 = @gitlab-workhorse;
    return 418;
  }

  location ~ ^/api/v3/projects/.*/repository/archive {
    # 'Error' 418 is a hack to re-use the @gitlab-workhorse block
    error_page 418 = @gitlab-workhorse;
    return 418;
  }

  # Build artifacts should be submitted to this location
  location ~ ^/[\w\.-]+/[\w\.-]+/builds/download {
      client_max_body_size 0;
      # 'Error' 418 is a hack to re-use the @gitlab-workhorse block
      error_page 418 = @gitlab-workhorse;
      return 418;
  }

  # Build artifacts should be submitted to this location
  location ~ /ci/api/v1/builds/[0-9]+/artifacts {
      client_max_body_size 0;
      # 'Error' 418 is a hack to re-use the @gitlab-workhorse block
      error_page 418 = @gitlab-workhorse;
      return 418;
  }

  # Build artifacts should be submitted to this location
  location ~ /api/v4/jobs/[0-9]+/artifacts {
      client_max_body_size 0;
      # 'Error' 418 is a hack to re-use the @gitlab-workhorse block
      error_page 418 = @gitlab-workhorse;
      return 418;
  }


  # For protocol upgrades from HTTP/1.0 to HTTP/1.1 we need to provide Host header if its missing
  if ($http_host = "") {
  # use one of values defined in server_name
    set $http_host_with_default "git.example.com";
  }

  if ($http_host != "") {
    set $http_host_with_default $http_host;
  }

  location @gitlab-workhorse {

    ## https://github.com/gitlabhq/gitlabhq/issues/694
    ## Some requests take more than 30 seconds.
    proxy_read_timeout      3600;
    proxy_connect_timeout   300;
    proxy_redirect          off;

    # Do not buffer Git HTTP responses
    proxy_buffering off;

    proxy_set_header    Host                $http_host_with_default;
    proxy_set_header    X-Real-IP           $remote_addr;
    proxy_set_header    X-Forwarded-For     $proxy_add_x_forwarded_for;
    proxy_set_header    X-Forwarded-Proto   $scheme;

    proxy_pass http://gitlab-workhorse;

    ## The following settings only work with NGINX 1.7.11 or newer
    #
    ## Pass chunked request bodies to gitlab-workhorse as-is
    # proxy_request_buffering off;
    # proxy_http_version 1.1;
  }

  ## Enable gzip compression as per rails guide:
  ## http://guides.rubyonrails.org/asset_pipeline.html#gzip-compression
  ## WARNING: If you are using relative urls remove the block below
  ## See config/application.rb under "Relative url support" for the list of
  ## other files that need to be changed for relative url support
  location ~ ^/(assets)/ {
    root /opt/gitlab/embedded/service/gitlab-rails/public;
    gzip_static on; # to serve pre-gzipped version
    expires max;
    add_header Cache-Control public;
  }

  error_page 502 /502.html;
}
```
别忘了在上面的示例中更新'git.example.com'作为你的服务器url。

**注意：** 如果你收到403禁止访问，你可能没有在/etc/nginx/nginx.conf中启用passenger，
简单的注释掉就行：

```
# include /etc/nginx/passenger.conf;
```
最后，运行'sudo service nginx reload'

### 启用/禁用nginx_status

默认情况下，你将拥有一个nginx健康检查端点，配置为127.0.0.1:8060/nginx_status，以监视你的nginx服务器状态。

#### 下面的信息将被展示：

```
Active connections: 1
server accepts handled requests
 18 18 36
Reading: 0 Writing: 1 Waiting: 0
```
* 激活的连接-所有开放的连接
* 3个数字被显示
 * 所有接收到的连接
 * 所有已握手的连接
 * 握手请求的总数
* Reading：Nginx读取请求头部
* Writing: Nginx读取请求体，处理请求，或向客户端写响应
* Waiting: Keep-alive连接。这个数字依赖keepalive-timeout。

## 配置

编辑`/etc/gitlab/gitlab.rb`:

```Ruby
nginx['status'] = {
  "listen_addresses" => ["127.0.0.1"],
  "fqdn" => "dev.example.com",
  "port" => 9999,
  "options" => {
    "stub_status" => "on", # Turn on stats
    "access_log" => "on", # Disable logs for stats
    "allow" => "127.0.0.1", # Only allow access from localhost
    "deny" => "all" # Deny access to anyone else
  }
}
```

如果你发现这个服务对你当前的架构没有用，你可以禁用它：

```ruby
nginx['status'] = {
  'enable' => false
}
```

确保运行sudo gitlab-ctl reconfigure使配置生效。



#### 警告

为保证用户可以上传，你的Nginx用户（通常是`www-data`）应该被加到`gitlab-www`组中。
可通过下面的命令行实现：

```shell
sudo usermod -aG gitlab-www www-data
```

#### 模板

除了Passenger配置代替了Unicorn和HTTPS的缺少（尽管可以被启用），其他文件基本上与内置配置相同：

- [bundled Gitlab Nginx configuration][nginx-cookbook]

不要忘了重启Nginx以启用新的配置（在基于Debian系统上用`sudo service nginx restart`）。

#### 故障排除

##### 400 Bad Request: too many Host Header
确保你在`nginx['custom_gitlab_server_config']`中没有对`proxy_set_header`进行配置，而是在`gitlab.rb`中配置
['proxy_set_headers'](https://docs.gitlab.com/omnibus/settings/nginx.html#supporting-proxied-ssl)。

##### javax.net.ssl.SSLHandshakeException: Received fatal alert: handshake_failure

从GitLab 10开始，omnibus-gitlab包默认不在支持TLSv1协议。这样在与一些较老的基于Java的IDE客户端进行交互时，将会导致连接问题。
我们强烈建议您在服务器上升级密码，就像在 [this user comment](https://gitlab.com/gitlab-org/gitlab-ce/issues/624#note_299061)中提到的一样

如果不可能修改服务器，你可以在`/etc/gitlab/gitlab.rb`中将缺省值设为旧的协议：

```
nginx['ssl_protocols'] = "TLSv1 TLSv1.1 TLSv1.2"
```

[recipes-web]: https://gitlab.com/gitlab-org/gitlab-recipes/tree/master/web-server
[selinuxmod]: https://gitlab.com/gitlab-org/gitlab-recipes/tree/master/web-server/apache#selinux-modifications
[http2 protocol]: https://tools.ietf.org/html/rfc7540
[http2 whitepaper]: https://assets.wp.nginx.com/wp-content/uploads/2015/09/NGINX_HTTP2_White_Paper_v4.pdf?_ga=1.127086286.212780517.1454411744
[http2 cipher blacklist]: https://tools.ietf.org/html/rfc7540#appendix-A
[nginx-cookbook]: https://gitlab.com/gitlab-org/omnibus-gitlab/tree/master/files/gitlab-cookbooks/gitlab/templates/default/nginx-gitlab-http.conf.erb
