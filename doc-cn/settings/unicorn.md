# Unicorn设置

如果你需要适配Unicorn超时或工作进程（？the number of workers）
 你可以在`/etc/gitlab/gitlab.rb`中使用下面的设置。
 运行`sudo gitlab-ctl reconfigure`是配置生效。

```ruby
unicorn['worker_processes'] = 3
unicorn['worker_timeout'] = 60
```

> 注意：文本编辑器正常运行的`worker_processes`最低配置是2，详见 [gitlab-ce#18771](https://gitlab.com/gitlab-org/gitlab-ce/issues/18771)
