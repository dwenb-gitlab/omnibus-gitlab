# SSL设置

Omnibus-gitlab附带官方的 [CAcert.org](http://www.cacert.org/)，
用于验证可信根认证机构的集合证书的真实性。

对于使用自签名证书或自定义证书的安装，Omnibus-gitlab提供管理这些证书的方法。
想了解更多的工作原理细节，详见这一页底部的 [details](#details-on-how-gitlab-and-ssl-work)。

## 安装自定义认证机构

从GitLab *8.9* 版本开始，omnibus-gitlab 包将可以处理定制的证书。

1. 将自定义(根CA)或自签名证书放在 `/etc/gitlab/trusted-certs/` 目录；
例如， `/etc/gitlab/trusted-certs/customcacert.pem`。
**注意**：证书必须是 **DER- 或 PEM-编码的**。
1. 运行 `gitlab-ctl reconfigure`。

这将在 `/opt/gitlab/embedded/ssl/certs/` 中创建一个指向你的自定义证书的符号链接。
符号链接名是该主题哈希。
**警告** 在 `/opt/gitlab/embedded/ssl/certs` 中发现的任何损坏的符号链接都将被删除，
删除后，任何现有符号链接都不会更改。
如果目录包含有效的证书，它们将被自动移动到 `/etc/gitlab/trusted-certs`。
如果目录包含任何其他文件，重新配置将运行失败，并附带错误信息:

```
ERROR: Not a certificate: /opt/gitlab/embedded/ssl/certs/FILE -> /opt/gitlab/embedded/ssl/certs/FILE
```

将非证书的文件移出 `/opt/gitlab/embedded/ssl/certs`
并再次运行reconfigure。

**警告** 在GitLab 8.9.0、8.9.1和8.9.2版本中，用来持有自定义证书的目录
被错误的设置为 `/etc/gitlab/ssl/trusted-certs/`。
如果你在这个目录里 **没有** 任何文件，那个删除它很安全。
如果你有自定义证书在里面，将他们移到 `/etc/gitlab/trusted-certs/` 
然后运行 `sudo gitlab-ctl reconfigure`。

[CAcert.org]: http://www.cacert.org/

## 疑难排解

如果在 `/opt/gitlab/embedded/ssl/certs/` 中没有创建符号链接，
但是你在运行 `gitlab-ctl reconfigure`之后看到"Skipping `cert.pem`"信息，
这意味着可能存在以下两个问题之一：

1. 在 `/etc/gitlab/ssl/trusted-certs/` 中的文件是一个符号链接
2. 该文件不是有效的PEM或DER编码的证书

要测试证书是否是有效的PEM格式，可以运行
`openssl`来解码证书。例如：

```
/opt/gitlab/embedded/bin/openssl x509 -in /etc/gitlab/trusted-certs/example.pem -text -noout
```

测试证书是否为有效的DER格式：

```
/opt/gitlab/embedded/bin/openssl x509 -inform DER -in /etc/gitlab/trusted-certs/example.der -text -noout
```

有效证书的输出如下：

```
Certificate:
        Data:
            Version: 1 (0x0)
            Serial Number: 3578 (0xdfa)
        Signature Algorithm: sha1WithRSAEncryption
            Issuer: C=JP, ST=Tokyo, L=Chuo-ku, O=Frank4DD, OU=WebCert Support, CN=Frank4DD Web CA/emailAddress=support@frank4dd.com
            Validity
                Not Before: Aug 22 05:26:54 2012 GMT
                Not After : Aug 21 05:26:54 2017 GMT
            Subject: C=JP, ST=Tokyo, O=Frank4DD, CN=www.example.com
            Subject Public Key Info:
                Public Key Algorithm: rsaEncryption
                    Public-Key: (512 bit)
                    Modulus:
                        00:9b:fc:66:90:79:84:42:bb:ab:13:fd:2b:7b:f8:
                        de:15:12:e5:f1:93:e3:06:8a:7b:b8:b1:e1:9e:26:
                        bb:95:01:bf:e7:30:ed:64:85:02:dd:15:69:a8:34:
                        b0:06:ec:3f:35:3c:1e:1b:2b:8f:fa:8f:00:1b:df:
                        07:c6:ac:53:07
                    Exponent: 65537 (0x10001)
        Signature Algorithm: sha1WithRSAEncryption
             14:b6:4c:bb:81:79:33:e6:71:a4:da:51:6f:cb:08:1d:8d:60:
             ec:bc:18:c7:73:47:59:b1:f2:20:48:bb:61:fa:fc:4d:ad:89:
             8d:d1:21:eb:d5:d8:e5:ba:d6:a6:36:fd:74:50:83:b6:0f:c7:
             1d:df:7d:e5:2e:81:7f:45:e0:9f:e2:3e:79:ee:d7:30:31:c7:
             20:72:d9:58:2e:2a:fe:12:5a:34:45:a1:19:08:7c:89:47:5f:
             4a:95:be:23:21:4a:53:72:da:2a:05:2f:2e:c9:70:f6:5b:fa:
             fd:df:b4:31:b2:c1:4a:9c:06:25:43:a1:e6:b4:1e:7f:86:9b:
             16:40
```

一个无效的文件将显示如下内容：

```
unable to load certificate
140663131141784:error:0906D06C:PEM routines:PEM_read_bio:no start line:pem_lib.c:701:Expecting: TRUSTED CERTIFICATE
```

## GitLab和SSL工作原理细节

GitLab-Omnibus包含它自己的OpenSSL库，并针对这个库链接所有已编译程序（例如， Ruby, PostgreSQL, 等等）。
编译这个库是在 `/opt/gitlab/embedded/ssl/certs` 中寻找证书。

GitLab-Omnibus通过对任何证书进行符号链接来管理自定义证书，
使用 [c_rehash](https://www.openssl.org/docs/man1.1.0/apps/c_rehash.html)工具从
`/etc/gitlab/trusted-certs/` 添加到 `/opt/gitlab/embedded/ssl/certs`。
例如，假设我们添加 `customcacert.pem` 到 `/etc/gitlab/trusted-certs/`：

```
$ sudo ls -al /opt/gitlab/embedded/ssl/certs
total 272
drwxr-xr-x 2 root root   4096 Jul 12 04:19 .
drwxr-xr-x 4 root root   4096 Jul  6 04:00 ..
lrwxrwxrwx 1 root root     42 Jul 12 04:19 7f279c95.0 -> /etc/gitlab/trusted-certs/customcacert.pem
-rw-r--r-- 1 root root 263781 Jul  5 17:52 cacert.pem
-rw-r--r-- 1 root root    147 Feb  6 20:48 README
```

这里我们看到了链接到自定义证书的证书指纹是 `7f279c95`。

当我们发出HTTPS请求时会发生什么?让我们运行一个简单的Ruby程序：

```ruby
#!/opt/gitlab/embedded/bin/ruby
require 'openssl'
require 'net/http'

Net::HTTP.get(URI('https://www.google.com'))
```

这是幕后发生的事情：

1.  "require `openssl`" 行引起解释器加载 `/opt/gitlab/embedded/lib/ruby/2.3.0/x86_64-linux/openssl.so`。
2. `Net::HTTP` 调用尝试读取在 `/opt/gitlab/embedded/ssl/certs/cacert.pem` 中的默认证书包。
3. SSL 谈判发生。
4. 服务器发送它的SSL证书。
4. 如果发送的证书由包覆盖，则SSL将成功完成。
5. 否则，OpenSSL可以通过搜索文件来验证在预定义的证书目录中匹配它们指纹的其他证书。
例如，如果证书具有指纹' 7f279c95 '， OpenSSL将尝试读取 `/opt/gitlab/embedded/ssl/certs/7f279c95.0`。

注意，OpenSSL库支持 `SSL_CERT_FILE` 和 `SSL_CERT_DIR` 环境变量的定义。前者定义默认值
要加载的证书包，而后者定义了要加载的目录用来搜索更多证书。如果你已经将证书添加到
`trusted-certs` 目录，这些变量不是必须要的。然而，如果因为什么原因你需要设置他们，
他们可以被[定义为环境变量](environment-variables.md)。例如：

```ruby
gitlab_rails['env'] = {"SSL_CERT_FILE" => "/usr/lib/ssl/private/customcacert.pem"}
```
