# 备份

## 备份和恢复Omnibus GitLab配置

建议在安全的地方保持一个 `/etc/gitlab` 备份，或者至少一个 `/etc/gitlab/gitlab-secrets.json` 备份。
如果你需要还原Gitlab应用程序备份，你还需要还原 `/etc/gitlab/gitlab-secrets.json`。
如果没有，则使用双因素身份验证的GitLab用户将失去对GitLab服务器的访问权，
存储在GitLab CI中的'secure variables'也将会丢失。

不建议将配置备份存储在与应用程序数据备份的位置相同，请参见下面。

omnibus-gitlab的所有配置都存储在 `/etc/gitlab` 中。备份你的配置，只需备份此目录。

```shell
# Example backup command for /etc/gitlab:
# Create a time-stamped .tar file in the current directory.
# The .tar file will be readable only to root.
sudo sh -c 'umask 0077; tar -cf $(date "+etc-gitlab-%s.tar") -C / etc/gitlab'
```

要创建每日应用程序备份，请编辑用户根目录的cron表：

```shell
sudo crontab -e -u root
```

cron表将出现在编辑器中。

输入命令创建一个压缩tar文件，其中包含 `/etc/gitlab/`。 例如，
将备份安排在每周的周二(第2天)到周六(第6天)之后的每天早上运行:


```
15 04 * * 2-6  umask 0077; tar cfz /secret/gitlab/backups/$(date "+etc-gitlab-\%s.tgz") -C / etc/gitlab

```

[cron is rather particular](http://www.pantz.org/software/cron/croninfo.html)
关于cron表，注意：

- 命令后的空行
- 转义百分比字：\%

您可以提取.tar文件，如下所示。

```shell
# Rename the existing /etc/gitlab, if any
sudo mv /etc/gitlab /etc/gitlab.$(date +%s)
# Change the example timestamp below for your configuration backup
sudo tar -xf etc-gitlab-1399948539.tar -C /
```

记得在恢复备份配置后运行 `sudo gitlab-ctl reconfigure`。

注意：你机器的SSH主机秘钥存在 `/etc/ssh/` 的单独位置，如果必须执行完整的机器恢复，请确保[备份和恢复这些秘钥](https://superuser.com/questions/532040/copy-ssh-keys-from-one-server-to-another-server/532079#532079)

### 从应用程序数据中分离配置备份

不要将你的应用程序备份（Git repositories, SQL data）和你的配置备份存放在相同的地方（`/etc/gitlab`）。
`gitlab-secrets.json` 文件（也可能是 `gitlab.rb` 文件）包含数据加密秘钥，以保护在SQL数据库中的敏感数据：

- GitLab two-factor authentication (2FA) user secrets ('QR codes')
- GitLab CI 'secure variables'

如果将配置备份与应用程序数据备份分开，就可以减少加密的应用程序数据丢失/泄漏/被盗的几率，
同时还可以减少解密所需的密钥。

## 创建应用程序备份

要创建存储库和GitLab元数据的备份，To create a backup of your repositories and GitLab metadata, 遵循
[备份创建文档](https://docs.gitlab.com/ce/raketasks/backup_restore.html#creating-a-backup-of-the-gitlab-system).

备份创建将在 `/var/opt/gitlab/backups` 中存储一个tar文件。

如果您想将GitLab备份存储在不同的目录中，在 `/etc/gitlab/gitlab.rb` 中添加下面的设置，并运行 `sudo gitlab-ctl reconfigure`：

```ruby
gitlab_rails['backup_path'] = '/mnt/backups'
```

## 在Docker容器中为GitLab实例创建备份

可以通过在命令前天添加 `docker exec -t <your container name>` 在主机上计划备份。

备份应用程序：

```shell
docker exec -t <your container name> gitlab-rake gitlab:backup:create
```

备份配置和加密（secrets）：

```shell
docker exec -t <your container name> /bin/sh -c 'umask 0077; tar cfz /secret/gitlab/backups/$(date "+etc-gitlab-\%s.tgz") -C / etc/gitlab'
```

>**注意：**
你需要有卷挂载在 `/secret/gitlab/backups` 和 `/var/opt/gitlab` 上，以便将这些备份保存在容器之外。

## 恢复应用程序备份

详见 [备份恢复文档](https://docs.gitlab.com/ce/raketasks/backup_restore.html#restore-for-omnibus-installations).

## 使用非打包数据库（non-packaged database）进行备份和恢复

如果你使用非打包数据库详见 [非打包数据库文档](database.md#using-a-non-packaged-postgresql-database-management-server).

## 上传备份到远程(云)存储U

详细参考 [Gitlab CE 备份恢复文档](https://docs.gitlab.com/ce/raketasks/backup_restore.html#uploading-backups-to-a-remote-cloud-storage).

## 手动管理备份目录

Omnibus-gitlab使用 `gitlab_rails['backup_path']` 创建备份目录。
该目录由运行GitLab的用户拥有，它具有严格的权限设置，只有该用户可以访问。该目录将保存备份档案，其中包含敏感信息。
在某些组织中，权限需要有所不同，这是因为，例如，将备份存档发送到站点之外。

禁用备份目录管理，在 `/etc/gitlab/gitlab.rb` 中设置：

```ruby
gitlab_rails['manage_backup_path'] = false
```
*警告* 如果您设置了这个配置选项，将由你在 `gitlab_rails['backup_path']` 中指定创建的目录并设置权限，
这将允许在 `user['username']` 中的用户拥有正确的访问权限。如果不这样做，GitLab将无法创建备份存档。