# Redis设置

## 使用未打包的Redis实例

如果你想使用自己的Redis实例来而不是自带的Redis实例，你可以使用下面的`gitlab.rb`设置。
运行`gitlab-ctl reconfigure`使设置生效。

```ruby
redis['enable'] = false

# Redis via TCP
gitlab_rails['redis_host'] = 'redis.example.com'
gitlab_rails['redis_port'] = 6380

# OR Redis via Unix domain sockets
gitlab_rails['redis_socket'] = '/tmp/redis.sock' # defaults to /var/opt/gitlab/redis/redis.socket
```

## 使绑定的Redis实例可以通过TCP访问

如果你想创建一个由omnibus-gitlab通过TCP进行管理的Redis实例，
可以使用下面的设置。

```ruby
redis['port'] = 6379
redis['bind'] = '127.0.0.1'
```

## 设置只做缓存（Redis-only？）服务器

如果为了使用Gitlab你想安装一个独立的Redis服务器（例如，在可伸缩的情况下？）
你可以使用GitLab Omnibus这么做：

> **注意：** 默认情况下，Redis不需要身份验证。见[Redis安全](http://redis.io/topics/security)文档查看更多信息。
  我们推荐使用Redis密码和严密的的防火墙规则组合来保护你的Redis服务。

1. 使用**步骤1和2**从[GitLabdownloads](https://about.gitlab.com/downloads)下载/安装GitLab Omnibus。
   不要完成下载页面上的其他步骤。
1. 使用下面的配置创建/编辑`/etc/gitlab/gitlab.rb`。
   确保更改`external_url`以匹配最终的GitLab前端URL：

    ```ruby
    external_url 'https://gitlab.example.com'

    # Disable all services except Redis
    redis_master_role['enable'] = true

    # Redis configuration
    redis['port'] = 6379
    redis['bind'] = '0.0.0.0'

    # If you wish to use Redis authentication (recommended)
    redis['password'] = 'Redis Password'
    gitlab_rails['redis_password'] = 'Redis Password'
    
    # Disable automatic database migrations
    #   Only the primary GitLab application server should handle migrations
    gitlab_rails['auto_migrate'] = false
    ```
    
    > **注意：** `redis_master_role['enable']`选项仅作为GitLab 8.14的变量，详见
    [`gitlab_rails.rb`](https://gitlab.com/gitlab-org/omnibus-gitlab/blob/master/files/gitlab-cookbooks/gitlab/libraries/gitlab_rails.rb)
    了解通过该选项自动禁用哪些服务。

1. 运行`sudo gitlab-ctl reconfigure`安装配置。

## 在默认值之外增加Redis连接的数量

默认情况Redis只能接受10,000个客户端连接。如果你需要超过10,000个连接，
设置'maxclients'属性来适应你的需求。请注意，调整maxclients属性意味着
你需要考虑fs.file-max(即，"sysctl -w fs.file-max=20000")的系统设置。

```ruby
redis['maxclients'] = 20000
```

## 为Redis调整TCP堆栈

以下设置用于启用性能更好的Redis服务器实例。 'tcp_timeout'是
Redis服务器在终止空闲TCP连接之前等待的秒值。'tcp_keepalive'
是在没有通信的情况下，以秒为单位对TCP ack到客户端的可调设置。

```ruby
redis['tcp_timeout'] = "60"
redis['tcp_keepalive'] = "300"
```

## Redis HA设置

详见 <https://docs.gitlab.com/ce/administration/high_availability/redis.html>.
