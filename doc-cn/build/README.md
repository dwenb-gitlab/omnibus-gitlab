# 准备构建环境

有关如何使用Docker准备构建环境的说明，详见[准备构建环境页](prepare-build-environment.md)。

## 用法

### Build

你可以使用`build`命令创建特定平台的包：

```shell
$ bin/omnibus build gitlab
```

创建包的平台/架构类型将与调用`build project`命令的平台匹配。
所以在MacBook Pro上运行这个命令将生成一个特定于Mac OS X的包。构建完成后，可在`pkg/`下找到包。 

### Clean

使用`clean`命令，你可以清理构建过程中产生的所有临时文件：

```shell
$ bin/omnibus clean
```

使用`--purge`选项可以移除构建过程中包括安装目录(`/opt/gitlab`)和包缓存目录(`/var/cache/omnibus/pkg`)的__所有__文件：


```shell
$ bin/omnibus clean --purge
```

### Help

通过`help`命令，可访问Omnibus命令行接口完整帮助：

```shell
$ bin/omnibus help
```

## 构建Docker镜像

```shell
# Build with stable packagecloud packages
# This will build gitlab-ee (8.0.2-ee.1) using STABLE repo and tag it as gitlab-ee:latest
make docker_build RELEASE_VERSION=8.0.2-ee.1 PACKAGECLOUD_REPO=gitlab-ee RELEASE_PACKAGE=gitlab-ee

# Build with unstable packagecloud packages
# This will build gitlab-ce (8.0.2-ce.1) using UNSTABLE repo and tag it as gitlab-ce:latest
make docker_build RELEASE_VERSION=8.0.2-ce.1 PACKAGECLOUD_REPO=unstable RELEASE_PACKAGE=gitlab-ce
```

### 发布Docker镜像

```shell
# This will push gitlab-ee:latest as gitlab/gitlab-ee:8.0.2-ee.1
make docker_push RELEASE_PACKAGE=gitlab-ee RELEASE_VERSION=8.0.2-ee.1

# This will push gitlab-ce:latest as gitlab/gitlab-ce:8.0.2-ce.1
make docker_push RELEASE_PACKAGE=gitlab-ce RELEASE_VERSION=8.0.2-ce.1

# This will push gitlab-ce:latest as gitlab/gitlab-ce:latest
make docker_push_latest RELEASE_PACKAGE=gitlab-ce
```

## 通过自定义分支构建包

>**注意** 对于如何构建官方omnibus-gitlab包的说明，相见[发布过程](../release/README.md)文档。

如果你正在实现Gitlab组件中的一个特性，为了使用omnibus-gitlab包测试这个特性你可能需要使用自定义分支构建一个包。

例如，你已经在Gitlab Rails应用程序内实现了一些东西并且代码位于名叫`my-feature`的分支中。

要使用定制的分支来构建一个omnibus-gitlab包，您将需要
将分支名称放入omnibus-gitlab存储库中的`VERSION` 文件中。

同样的方法也适用于指定提交。如果你想构建一个这样的包
将使用特定的提交，你必须将该提交的SHA放在
VERSION文件。

例如，如果你想使用名为`my-feature-branch`分支构建一个包，omnibus-repo中的`VERSION`文件应该包含文本`my-feature-branch`。
类似的，如果你想使用特定的提交构建一个包，比如[这个](https://dev.gitlab.org/gitlab/gitlabhq/commit/46973f3d4602c7ea6366d6401116b89d72b83b9e)，
`VERSION`文件应该包含文本`46973f3d4602c7ea6366d6401116b89d72b83b9e`，这就是提交的SHA。

同样的，你可以对`GITLAB_WORKHORSE_VERSION`等执行同样的操作。

**注意：** 此自定义分支的名称不应与SemVer版本的格式匹配，即“xxx .yy.zz”。
这是因为omnibus-gitlab会在分支名称前添加一个`v`，将其误认为版本标记。
例如，分支名称不能为`0.5.0`，因为omnibus-gitlab将自动将其设置为`v0.5.0`。

## 构建EE包

要构建Gitlab EE包，在开始构建之前将环境变量`ee`设置为true（运行命令`$ export ee=true`）。
它将使omnibus-gitlab拉取EE仓库而不是CE库，然后构建EE包。

### GitLab公司开发人员注意事项

如果你是Gitlab公司团队的一员，那么你将有权访问构建基础设施(或者是能够访问基础设施的同事)。

你可以轻松的利用构建基础设施构建定制包来测试你的代码。

在你开始之前，
**你需要有omnibus-gitlab存储库的push权限。**

如果你有（某个人的）权限，你需要：

1. 确保你的自定义分支和`dev.gitlab.org`工程镜像同步。例如，你在`gitlab-shell`开发，
确保你的自定义分支被推送到`dev.gitlab.org`上的`gitlab-shell`存储库
1. 在omnibus-gitlab仓库上创建一个分支
1. 在这个分支中，打开组件的相关版本文件并指定
   分支的名称。例如你在`gitlab-shell`上开发，打开`GITLAB_SHELL_VERSION`并编写`my-feature`
1. 提交并推送omnibus-gitlab分支至`dev.gitlab.org`

这将触发自定义包的构建。如果构建成功，你将在构建跟踪的地步看到一个能够下载自定义包的链接。

您还可以使用CI构建基础结构更改构建输出的冗长性。在omnibus-gitlab工程设置(在dev.gitlab.org上)的CI/CD变量中，
添加带有`debug`的`BUILD_LOG_LEVEL`变量，并运行管道。
