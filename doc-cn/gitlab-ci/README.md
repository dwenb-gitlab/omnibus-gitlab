# GitLab CI

从GitLab 8.0开始，GitLab CI被集成在GitLab中。

查看如何使用GitLab CI配置项目，见 [GitLab CI快速启动文档](https://docs.gitlab.com/ce/ci/quick_start/README.html).
