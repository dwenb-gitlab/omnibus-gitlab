# 手动下载安装Gitlab包

如果你想使用GitLab官方 [包仓库](https://about.gitlab.com/installation)，你可以手动下载安装Omnibus Gitlab包。

## 下载GitLab包

所有的GitLab包都被发布到[包服务器](https://packages.gitlab.com/gitlab/)并且可以被下载，我们维护五个仓库：

* [GitLab EE](https://packages.gitlab.com/gitlab/gitlab-ee)：官方企业版
* [GitLab CE](https://packages.gitlab.com/gitlab/gitlab-ce)：官方社区版
* [Unstable](https://packages.gitlab.com/gitlab/unstable)：候选版本和其他不稳定版本
* [Nighty Builds](https://packages.gitlab.com/gitlab/nightly-builds)：每日构建版（原文：for nightly builds）
* [Raspberry Pi 2](https://packages.gitlab.com/gitlab/raspberry-pi2)：[Raspberry Pi 2](https://www.raspberrypi.org)包

浏览你想要的包仓库地址，查看可用的包列表。每个版本都有多个包，每个包对应一个受支持的发行版类型。文件名旁边是一个标签，表示分发版，因为文件名可能是相同的。

![包列表](img/package_list.png)

找到想要使用的版本和发行版所需的包，并单击要下载的文件名。

## 安装GitLab包

下载所需的包后，使用您的系统包管理工具来安装它。例如：

* DEB based (Ubuntu, Debian, Raspberry Pi): `sudo EXTERNAL_URL="http://gitlab.example.com" dpkg -i gitlab-ee-9.5.2-ee.0_amd64.deb`
* RPM based (CentOS, RHEL, Oracle, Scientific, openSUSE, SLES): `sudo EXTERNAL_URL="http://gitlab.example.com" rpm -i gitlab-ee-9.5.2-ee.0.el7.x86_64.rpm`

修改 `http://gitlab.example.com` 为你想接入Gitlab实例的URL。安装将会自动配置，并在启动后使用刚才修改的URL。

> **注意：** 启用HTTPS需要 [增加配置](settings/nginx.html#enable-https) 指定证书。

## 浏览到主机名并登录

第一次访问时，您将被重定向到密码重置页。提供初始管理员帐户的密码，您将被重定向回登录页，然后使用默认帐户的用户名' root '登录。

参阅 [安装和配置详细说明](https://docs.gitlab.com/omnibus/README.html#installation-and-configuration-using-omnibus-package).
