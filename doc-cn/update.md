# 通过omnibus-gitlab更新Gitlab

## 文档版本

请确保你在master分支上预览该文件。

#### 从GitLab 6.6或更高版本升级到7.10或更新的版本

详见[doc/update/README.md.](update/README.md#updating-from-gitlab-66-and-higher-to-710-or-newer)

#### 从GitLab 6.6或更高版本升级到最新版本

详见[doc/update/README.md.](update/README.md#updating-from-gitlab-66-and-higher-to-the-latest-version)

#### 从gitlab 6.6.0.pre1更新到6.6.4

See [doc/update/README.md.](update/README.md#updating-from-gitlab-660pre1-to-664)

#### 恢复到Gitlab 6.6.x或更高版本

详见[doc/update/README.md.](update/README.md#reverting-to-gitlab-66x-or-later)

#### 从non-Omnibus安装升级到Omnibus安装

详见[doc/update/README.md.](update/README.md#upgrading-from-a-non-omnibus-installation-to-an-omnibus-installation)

#### 使用备份从non-Omnibus PostgreSQL升级到Omnibus安装 

详见[doc/update/README.md.](update/README.md#upgrading-from-non-omnibus-postgresql-to-an-omnibus-installation-using-a-backup)

#### 从non-Omnibus PostgreSQL原状（in-place）升级到Omnibus安装

详见[doc/update/README.md.](update/README.md#upgrading-from-non-omnibus-postgresql-to-an-omnibus-installation-in-place)

#### 从non-Omnibus MySQL升级到Omnibus安装（6.8+版本）

详见[doc/update/README.md.](update/README.md#upgrading-from-non-omnibus-mysql-to-an-omnibus-installation-version-68)

#### RPM 'package is already installed' （包已安装）错误

详见[doc/update/README.md.](update/README.md#rpm-package-is-already-installed-error)

#### 通过omnibus-gitlab升级GitLab CI
#### 从5.4.0之前的Gitlab CI版本更新到最新版本

详见[doc/update/README.md.](update/README.md#updating-from-gitlab-ci-version-prior-to-540-to-the-latest-version)
