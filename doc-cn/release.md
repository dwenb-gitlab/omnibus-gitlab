#### Omnibus-gitlab发布过程
迁移至[release/README.md.](release/README.md)

#### 开发环境

详见[release/README.md.](release/README.md#on-your-development-machine)

#### 发布包

详见[release/README.md.](release/README.md#publishing-the-packages)
