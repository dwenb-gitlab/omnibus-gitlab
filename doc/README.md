---
toc: false
---

# Omnibus GitLab文档

Omnibus是一种打包运行GitLab所需的不同服务和工具的方法，以便大多数用户在不需要了解费劲的配置情况下安装它。

## 封装信息

- [查看封装的软件版本](package-information/README.md#checking-the-versions-of-bundled-software)
- [默认配置](package-information/defaults.md)
- [不支持的操作系统](package-information/deprecated_os.md)
- [软件包签名信息](package-information/signed_packages.md)
- [已废弃的策略](package-information/deprecation_policy.md)

## 安装

##### 先决条件

- [安装需求](https://docs.gitlab.com/ce/install/requirements.html)
- 如果你想通过域名访问自建的 GitLab，如：mygitlabinstance.com，请确保确保域名正确的解析到已经安装了 GitLab 的服务器 IP 。你可以使用命令 `host mygitlabinstance.com` 检测解析出来的 IP 是否正确。
- 如果你想给自建的 GitLab 增加 HTTPS 访问协议，请确保已经准备好该域名的 SSL 证书。（需要注意的是 GitLab 的其他组件，如容器注册中心，可以设置独立的二级域名，这些域名与需要配置 SSL 证书。）
- 如果你想让自建的 GitLab 发送通知邮件，请安装配置邮件服务器，比如 sendmail 。同时 GitLab 也支持第三方 SMTP 服务器，下文将会详细介绍支持的 SMTP 服务。

#### 使用omnibus包进行安装和配置

**`注意`**: 本节描述的是常用的配置设置。 完整的配置设置查看 [配置](#configuring) 章节。

* [安装 GitLab](https://about.gitlab.com/installation/)
  * [手动下载安装 GitLab 包](manual_install.md)
* [设置域名/URL](https://docs.gitlab.com/omnibus/settings/configuration.html#configuring-the-external-url-for-gitlab) 为Gitlab实例设置，以使它更方便的被访问
* [启用 HTTPS](https://docs.gitlab.com/omnibus/settings/nginx.html#enable-https)
* [启用邮件通知](https://docs.gitlab.com/omnibus/settings/smtp.html#smtp-settings)
* [启用通过电子邮件回复](https://docs.gitlab.com/ce/administration/reply_by_email.html#set-it-up)
    * [安装并配置 postfix](https://docs.gitlab.com/ce/administration/reply_by_email_postfix_setup.html)
* [在GitLab上启用容器注册](https://docs.gitlab.com/ce/administration/container_registry.html#container-registry-domain-configuration)
    * 容器注册中心使用的域需要SSL证书
* [启用 GitLab 页面](https://docs.gitlab.com/ce/administration/pages/)
    * 如果希望启用HTTPS，则必须获得通配符证书
* [启用 ElasticSearch](https://docs.gitlab.com/ee/integration/elasticsearch.html)
* [GitLab Mattermost](gitlab-mattermost/README.md) 设置与Omnibus GitLab包一起发布的Mattermost消息app。(译者注：此处跳转链接非绝对地址)
* [GitLab Prometheus](https://docs.gitlab.com/ce/administration/monitoring/performance/prometheus.html) S设置包含在Omnibus GitLab包中的 Prometheus 监控
* [GitLab 高可用性角色](roles/README.md) (译者注：此处跳转链接非绝对地址)

#### Using docker image

You can also use the docker images provided by GitLab to install and configure a GitLab instance. Check the [documentation](docker/README.md) to know more.

## Maintenance

- [Get service status](maintenance/README.md#get-service-status)
- [Starting and stopping](maintenance/README.md#starting-and-stopping)
- [Invoking Rake tasks](maintenance/README.md#invoking-rake-tasks)
- [Starting a Rails console session](maintenance/README.md#starting-a-rails-console-session)

## Configuring

- [Configuring the external url](settings/configuration.md#configuring-the-external-url-for-gitlab)
- [Configuring a relative URL for Gitlab (experimental)](settings/configuration.md#configuring-a-relative-url-for-gitlab)
- [Storing git data in an alternative directory](settings/configuration.md#storing-git-data-in-an-alternative-directory)
- [Changing the name of the git user group](settings/configuration.md#changing-the-name-of-the-git-user-group)
- [Specify numeric user and group identifiers](settings/configuration.md#specify-numeric-user-and-group-identifiers)
- [Only start omnibus-gitlab services after a given filesystem is mounted](settings/configuration.md#only-start-omnibus-gitlab-services-after-a-given-filesystem-is-mounted)
- [Disable user and group account management](settings/configuration.html#disable-user-and-group-account-management)
- [Disable storage directory management](settings/configuration.html#disable-storage-directories-management)
- [Configuring Rack attack](settings/configuration.md#configuring-rack-attack)
- [SMTP](settings/smtp.md)
- [NGINX](settings/nginx.md)
- [LDAP](settings/ldap.md)
- [Unicorn](settings/unicorn.md)
- [Redis](settings/redis.md)
- [Logs](settings/logs.md)
- [Database](settings/database.md)
- [Reply by email](https://docs.gitlab.com/ce/incoming_email/README.html)
- [Environment variables](settings/environment-variables.md)
- [gitlab.yml](settings/gitlab.yml.md)
- [Backups](settings/backups.md)
- [Pages](https://docs.gitlab.com/ce/pages/administration.html)
- [SSL](settings/ssl.md)

## Updating

- [Upgrade support policy](https://docs.gitlab.com/ee/policy/maintenance.html)
- [Upgrade from Community Edition to Enterprise Edition](update/README.md#from-community-edition-to-enterprise-edition)
- [Updating to the latest version](update/README.md#updating-from-gitlab-66-and-higher-to-the-latest-version)
- [Downgrading to an earlier version](update/README.md#reverting-to-gitlab-66x-or-later)
- [Upgrading from a non-Omnibus installation to an Omnibus installation using a backup](update/README.md#upgrading-from-non-omnibus-postgresql-to-an-omnibus-installation-in-place)
- [Upgrading from non-Omnibus PostgreSQL to an Omnibus installation in-place](update/README.md#upgrading-from-non-omnibus-postgresql-to-an-omnibus-installation-in-place)
- [Upgrading from non-Omnibus MySQL to an Omnibus installation (version 6.8+)](update/README.md#upgrading-from-non-omnibus-mysql-to-an-omnibus-installation-version-68)
- [RPM error: 'package is already installed' ](update/README.md#rpm-package-is-already-installed-error)
- [Note about updating from GitLab 6.6 and higher to 7.10 or newer](update/README.md#updating-from-gitlab-66-and-higher-to-710-or-newer)
- [Updating from GitLab 6.6.0.pre1 to 6.6.4](update/README.md#updating-from-gitlab-660pre1-to-664)
- [Updating from GitLab CI version prior to 5.4.0 to the latest version](update/README.md#updating-from-gitlab-ci-version-prior-to-540-to-the-latest-version)

## Troubleshooting

- [Hash Sum mismatch when installing packages](common_installation_problems/README.md#hash-sum-mismatch-when-installing-packages)
- [Apt error: 'The requested URL returned error: 403'](common_installation_problems/README.md#apt-error-the-requested-url-returned-error-403).
- [GitLab is unreachable in my browser](common_installation_problems/README.md#gitlab-is-unreachable-in-my-browser).
- [Emails are not being delivered](common_installation_problems/README.md#emails-are-not-being-delivered).
- [Reconfigure freezes at ruby_block[supervise_redis_sleep] action run](common_installation_problems/README.md#reconfigure-freezes-at-ruby_blocksupervise_redis_sleep-action-run).
- [TCP ports for GitLab services are already taken](common_installation_problems/README.md#tcp-ports-for-gitlab-services-are-already-taken).
- [Git SSH access stops working on SELinux-enabled systems](common_installation_problems/README.md#git-ssh-access-stops-working-on-selinux-enabled-systems).
- [Postgres error 'FATAL:  could not create shared memory segment: Cannot allocate memory'](common_installation_problems/README.md#postgres-error-fatal-could-not-create-shared-memory-segment-cannot-allocate-memory).
- [Reconfigure complains about the GLIBC version](common_installation_problems/README.md#reconfigure-complains-about-the-glibc-version).
- [Reconfigure fails to create the git user](common_installation_problems/README.md#reconfigure-fails-to-create-the-git-user).
- [Failed to modify kernel parameters with sysctl](common_installation_problems/README.md#failed-to-modify-kernel-parameters-with-sysctl).
- [I am unable to install omnibus-gitlab without root access](common_installation_problems/README.md#i-am-unable-to-install-omnibus-gitlab-without-root-access).
- [gitlab-rake assets:precompile fails with 'Permission denied'](common_installation_problems/README.md#gitlab-rake-assetsprecompile-fails-with-permission-denied).
- ['Short read or OOM loading DB' error](common_installation_problems/README.md#short-read-or-oom-loading-db-error).
- ['pg_dump: aborting because of server version mismatch'](settings/database.md#using-a-non-packaged-postgresql-database-management-server)
- ['Errno::ENOMEM: Cannot allocate memory' during backup or upgrade](common_installation_problems/README.md#errnoenomem-cannot-allocate-memory-during-backup-or-upgrade)
- [NGINX error: 'could not build server_names_hash'](common_installation_problems/README.md#nginx-error-could-not-build-server_names_hash-you-should-increase-server_names_hash_bucket_size)
- [Reconfigure fails due to "'root' cannot chown" with NFS root_squash](common_installation_problems/README.md#reconfigure-fails-due-to-root-cannot-chown-with-nfs-root_squash)

## Omnibus GitLab developer documentation

- [Development Setup](development/README.md)
- [Omnibus GitLab Architecture](architecture/README.md)
- [Adding a new Service to Omnibus GitLab](development/new-services.md)
- [Creating patches](development/creating-patches.md)
- [Release process](release/README.md)
- [Building your own package](build/README.md)
- [Building a package from a custom branch](build/README.md#building-a-package-from-a-custom-branch)
